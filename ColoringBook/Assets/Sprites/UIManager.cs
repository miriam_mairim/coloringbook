﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	[SerializeField] private Button _mLevelsButton = null;
	[SerializeField] private Button _mControlsButton = null;
	[SerializeField] private Button _mCreditsButton = null;
	[SerializeField] private Button _mExitButton = null;

	[SerializeField] private AudioSource _mButtonSound;

	private void Start()
	{
		Debug.Assert(_mLevelsButton != null);
		Debug.Assert(_mControlsButton != null);
		Debug.Assert(_mCreditsButton != null);
		Debug.Assert(_mExitButton != null);

		_mLevelsButton.onClick.AddListener(SelectLevel);
		_mControlsButton.onClick.AddListener(ShowControls);
		_mCreditsButton.onClick.AddListener(ShowCredits);
		_mExitButton.onClick.AddListener(ShowExit);

	}

	private void SelectLevel()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Levels");
	}

	private void ShowControls()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Controls");
	}

	private void ShowCredits()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Credits");
	}

	private void ShowExit()
	{
		_mButtonSound.Play();
		Application.Quit();
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}
