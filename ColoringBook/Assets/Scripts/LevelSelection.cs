﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour {

	[SerializeField] private Button _mLevel1 = null;
	[SerializeField] private Button _mLevel2 = null;
	[SerializeField] private Button _mLevel3 = null;

	[SerializeField] private AudioSource _mButtonSound;

	private void Start()
	{
		Debug.Assert(_mLevel1 != null);
		Debug.Assert(_mLevel2 != null);
		Debug.Assert(_mLevel3 != null);

		_mLevel1.onClick.AddListener(Level1);
		_mLevel2.onClick.AddListener(Level2);
		_mLevel3.onClick.AddListener(Level3);

	}

	private void Level1()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Level1_easy");
	}

	private void Level2()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Level2_medium");
	}

	private void Level3()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("Level3_hard");
	}
}
