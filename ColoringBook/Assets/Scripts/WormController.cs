﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormController : MonoBehaviour {

	[SerializeField] private SpriteRenderer _mPickedColor;


	public float MoveSpeed;
	public float JumpHeight;

	public Color PickedColor = Color.gray;

	public KeyCode LastHitKey;
	private Rigidbody2D _mRb;
	private bool _mOnAir;

	private SecondWormController _mWorm;

	private const float _maxSpeed = 10f;

	private Color _violet = new Color(212 / 255f, 55 / 255f, 153 / 255f, 1f);
	private Color _green = new Color(31 / 255f, 144 / 255f, 27 / 255f, 1f);
	private Color _orange = new Color(238 / 255f, 90 / 255f, 30 / 255f, 1f);

	public void Awake()
	{
		_mRb = GetComponent<Rigidbody2D>();
		_mWorm = FindObjectOfType<SecondWormController>();
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			_mOnAir = false;
			var c = col.gameObject.transform;
			if (c.childCount > 0)
			{
				if (Math.Abs(PickedColor.g - 47 / 255f) < 0.002f) //red
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.b > 200 / 255f) //blue
					{
						PickedColor = _violet;
						_mWorm.ColorTheBox();
					}
					else if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g > 200 / 255f) //yellow
					{
						PickedColor = _orange;
						_mWorm.ColorTheBox();
					}
				}
				else if (PickedColor.g > 200 / 255f) //yellow
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.b > 200 / 255f) //blue
					{
						PickedColor = _green;
						_mWorm.ColorTheBox();
					}
					else if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g < 100 / 255f) //red
					{
						PickedColor = _orange;
						_mWorm.ColorTheBox();
					}
				}
				else if (PickedColor.b > 200 / 255f) //blue
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g < 100 / 255f) //red
					{
						PickedColor = _violet;
						_mWorm.ColorTheBox();
					}
					else if(c.GetChild(0).GetComponent<SpriteRenderer>().color.g > 200 / 255f) //yellow
					{
						PickedColor = _green;
						_mWorm.ColorTheBox();
					}
				}
				else
				{
					PickedColor = c.GetChild(0).GetComponent<SpriteRenderer>().color;
					_mWorm.ColorTheBox();
				}
			}
			else
			{
				PickedColor = Color.gray;
			}

			_mPickedColor.color = PickedColor;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			var c = col.gameObject.transform;

			if (c.childCount > 0)
			{
				if (PickedColor == _violet)
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g > 200 / 255f) //yellow
					{
						PickedColor = new Color(221 / 255f, 47 / 255f, 16 / 255f);
						_mWorm.ColorTheBox();
					}
					else if(c.GetChild(0).GetComponent<SpriteRenderer>().color.g < 100 / 255f) //red
					{
						PickedColor = new Color(39 / 255f, 127 / 255f, 221 / 255f);
						_mWorm.ColorTheBox();
					}
				}
				else if (PickedColor == _orange)
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g > 200 / 255f) //yellow
					{
						PickedColor = new Color(221 / 255f, 47 / 255f, 16 / 255f);
						_mWorm.ColorTheBox();
					}
					else if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g < 100 / 255f) //red
					{
						PickedColor = new Color(255 / 255f, 253 / 255f, 64 / 255f);
						_mWorm.ColorTheBox();
					}
				}
				else if (PickedColor == _green)
				{
					if (c.GetChild(0).GetComponent<SpriteRenderer>().color.g > 200 / 255f) //yellow
					{
						PickedColor = new Color(221 / 255f, 47 / 255f, 16 / 255f);
						_mWorm.ColorTheBox();
					}
					else if (c.GetChild(0).GetComponent<SpriteRenderer>().color.b > 200 / 255f) //blue
					{
						PickedColor = new Color(255 / 255f, 253 / 255f, 64 / 255f);
						_mWorm.ColorTheBox();
					}
				}
				else
				{
					PickedColor = Color.gray;
				}
			}
			else
			{
				PickedColor = Color.gray;
			}
		}

		_mPickedColor.color = PickedColor;
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.W) && !_mOnAir)
		{
			_mRb.velocity = new Vector2(0, JumpHeight);
			_mOnAir = true;
		}
		else if (Input.GetKey(KeyCode.D))
		{
			if (LastHitKey != KeyCode.D)
			{
				transform.GetChild(0).transform.localScale = new Vector3(1,1,0);
			}

			_mRb.velocity = new Vector2(MoveSpeed, _mRb.velocity.y);
			LastHitKey = KeyCode.D;
		}
		else if (Input.GetKey(KeyCode.A))
		{
			if (LastHitKey != KeyCode.A)
			{
				transform.GetChild(0).transform.localScale = new Vector3(-1,1,0);
			}

			_mRb.velocity = new Vector2(-MoveSpeed, _mRb.velocity.y);
			LastHitKey = KeyCode.A;

		}

	}
}
