﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPickAndDrop : MonoBehaviour
{
	private SecondWormController _mWorm;
	private GameObject _mTile;
	private GameObject _mCarried;

	public void Awake()
	{
		_mWorm = FindObjectOfType<SecondWormController>();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			_mTile = col.gameObject;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (_mTile == col.gameObject)
		{
			_mTile = null;
		}	
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.RightShift))
		{
			if (_mCarried != null)
			{
				DropDownTiles();
			}
			else
			{
				if (_mTile != null && !_mTile.name.Contains("Book")) PickUpTiles(_mTile);
			}	
		}

	}

	void PickUpTiles(GameObject tile)
	{
		tile.transform.SetParent(_mWorm.transform);
		tile.transform.localPosition = Vector3.up *
		                               (tile.transform.GetChild(0).localScale.x == 2 ? 1.8f : 1.4f);
		_mCarried = tile;
		_mTile = null;
	}

	void DropDownTiles()
	{
		var isBig = _mCarried.transform.GetChild(0).localScale.x == 2f;
		_mCarried.transform.SetParent(null);
		var newX = _mWorm.LastHitKey == KeyCode.LeftArrow
			? _mWorm.transform.position.x - 1.2f * _mCarried.transform.GetChild(0).localScale.x
			: _mWorm.transform.position.x + 1.2f;
		newX = Mathf.Round(Mathf.Clamp(newX, 1f, Single.PositiveInfinity));
		_mCarried.transform.localPosition = new Vector3(newX, (float)Math.Round(_mWorm.transform.position.y));
		_mCarried = null;
		_mWorm.EvaluateLevel();
	}
}
