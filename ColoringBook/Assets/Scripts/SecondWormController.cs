﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SecondWormController : MonoBehaviour
{
	[SerializeField] private GameObject LevelCheckers;
	[SerializeField] private GameObject SuccessPanel;
	[SerializeField] private TextMeshProUGUI ScoreText;

	[SerializeField] private AudioSource _mSuccessSound;

	public float MoveSpeed;
	public float JumpHeight;

	public KeyCode LastHitKey;
	private Rigidbody2D _mRb;
	private bool _mOnAir;
	private WormController _mWorm;
	private Transform _mTouchedObject;

	private const float _maxSpeed = 10f;

	private int _mTileCount;

	public void Awake()
	{
		SuccessPanel.SetActive(false);
		_mRb = GetComponent<Rigidbody2D>();
		_mWorm = FindObjectOfType<WormController>();

		_mTileCount = LevelCheckers.transform.childCount;
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			_mOnAir = false;

			if(col.name.Contains("Book")) return;

			var c = col.gameObject.transform;
			if (c.childCount > 0)
			{
				_mTouchedObject = c.GetChild(0);
				ColorTheBox();
			}
			else
			{
				_mTouchedObject = null;
			}
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			_mTouchedObject = null;

		}
	}

	public void ColorTheBox()
	{
		if (Math.Abs(_mWorm.PickedColor.r - 0.5f) < 0.03f
		    && Math.Abs(_mWorm.PickedColor.g - 0.5f) < 0.03f
		    && Math.Abs(_mWorm.PickedColor.b - 0.5f) < 0.03f) return;

		if (_mTouchedObject != null && _mTouchedObject.GetComponent<SpriteRenderer>().color == _mWorm.PickedColor) return;

		if (_mTouchedObject != null && _mTouchedObject.GetComponent<SpriteRenderer>() != null)
		{
			_mTouchedObject.GetComponent<SpriteRenderer>().color = _mWorm.PickedColor;
		}

		EvaluateLevel();
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow) && !_mOnAir)
		{
			_mRb.velocity = new Vector2(0, JumpHeight);
			_mOnAir = true;
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			if (LastHitKey != KeyCode.RightArrow)
			{
				transform.GetChild(0).transform.localScale = new Vector3(1, 1, 0);
			}

			_mRb.velocity = new Vector2(MoveSpeed, _mRb.velocity.y);
			LastHitKey = KeyCode.RightArrow;
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			if (LastHitKey != KeyCode.LeftArrow)
			{
				transform.GetChild(0).transform.localScale = new Vector3(-1, 1, 0);
			}

			_mRb.velocity = new Vector2(-MoveSpeed, _mRb.velocity.y);
			LastHitKey = KeyCode.LeftArrow;

		}

	}

	public void EvaluateLevel()
	{
		for(var i = 0; i < LevelCheckers.transform.childCount; i++)
		{
			LevelCheckers.transform.GetChild(i).GetComponent<LevelChecker>().EvaluateSuccess();
		}

		var successCount = 0;


		for (var i = 0; i < LevelCheckers.transform.childCount; i++)
		{
			if(LevelCheckers.transform.GetChild(i).GetComponent<LevelChecker>().IsSuccess)
			{
				successCount++;
			}
		}


		ScoreText.SetText("Score: " + successCount + "/" + _mTileCount);


		if (successCount == LevelCheckers.transform.childCount)
		{
			_mSuccessSound.Play();
			SuccessPanel.SetActive(true);
		}
	}
}
