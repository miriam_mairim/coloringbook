﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChecker : MonoBehaviour
{
	[SerializeField] public Color TileColor;
	[SerializeField] public bool IsBigTile;

	public bool IsSuccess;

	private Transform _mTouchedObject;

	public void EvaluateSuccess()
	{
		if (_mTouchedObject == null)
		{
			IsSuccess = false;
			return;
		}

		if (_mTouchedObject.childCount > 0)
		{
			var colorOf = _mTouchedObject.GetChild(0).GetComponent<SpriteRenderer>().color;

			if (Math.Abs(colorOf.r - TileColor.r) < 0.03f
			    && Math.Abs(colorOf.g - TileColor.g) < 0.03f
			    && Math.Abs(colorOf.b - TileColor.b) < 0.03f)
			{
				IsSuccess = true;
			}
			else
			{
				IsSuccess = false;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			_mTouchedObject = col.gameObject.transform;
		}
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "FloorChecker")
		{
			EvaluateSuccess();
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "FloorChecker" && _mTouchedObject == col.gameObject.transform)
		{
			_mTouchedObject = null;
		}
	}

}
